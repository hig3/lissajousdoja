define(_AppName_,Lissajous)
define(_Title_,リサジュー図形)
define(_ShortDesc_,2つの単振動の作る図形)
define(_LongDesc_,`単振動とリサジュー図形の学習用デモです 振幅,振動数,初期位相の異なる2つの単振動を組み合わせると, リサジュー図形とよばれる美しい図形が現れます. ソースの大部分を共有する iアプリ版/Vアプリ版/EZアプリ版があります. そのソースを http://sparrow.math.ryukoku.ac.jp/~hig/info/mathfp/sample.zip で公開しています. 小数の計算には MathFP クラスを使っています。龍谷大学理工学部数理情報学科の授業で使用しています。')
define(_IMID_,021930)
define(_VFID_,021931)
define(_EZID_,021932)


