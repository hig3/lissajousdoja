/**
   リサジュー図形
   Time-stamp: "2004/07/16 Fri 10:12  hig"
   Saburo Higuchi 2004  http://www.math.ryukoku.ac.jp/~hig/
   Samples: http://sparrow.math.ryukoku.ac.jp/~hig/mobilejava/examples/
*/

// #define DEBUG 

// #define DOJA
// #define MIDP

// 次は, DoJa/MIDP Builder で禁止されている, #if #endif 間の#define を含みます.
// DoJa/MIDP Builder を使うときは, // でcomment out し一方だけを残しましょう.

// for DoCoMo i-appli
import com.nttdocomo.ui.*;
import com.nttdocomo.util.*;

import net.jscience.math.kvm.MathFP;

public class Lissajous extends IApplication  {

    // 起動するときに呼ばれるメソッド. 必須.

    public void start(){
	LissajousCanvas cc=new LissajousCanvas(this);
	Display.setCurrent(cc);
    }

}

class LissajousCanvas extends Canvas implements Runnable,

					     SoftKeyListener,ComponentListener 

{
    Lissajous parent;	// これを呼び出したオブジェクト

    final int zero=MathFP.toFP(0); // MathFP zero
    final int unity=MathFP.toFP(1); // MathFP one
    final int mille=MathFP.toFP(1000); // MathFP 1000
    // final int onerad=MathFP.div(MathFP.toFP(180),MathFP.PI); // π/180
    //    final int twopi=MathFP.mul(MathFP.PI,MathFP.toFP("2.0"));

    // 振動数と等価なパラメター ＋ 振幅
    int [] v;			// MathFP 下のパラメータ
    final String [] vname ={"振幅A1", "角速度ω1", "初期位相φ1/π","振幅A2", "角速度ω2", "初期位相φ2/π"};
    Harmonic [] h;

    final String [] formula = {"x(t)=A1 cos(ω1*t+φ1)",
			       "y(t)=A2 cos(ω2*t+φ2)"};

    // flags
    boolean inanim=false;	// アニメ中であるかどうか

    Thread rt;			// アニメ用スレッド
    int t=0;			// time counter. run() ごとに1ずつ増える.
    final int dt=100;		// millisecs  スレッドのスリープ時間
    int realsec;		// MathFP t in second

    // 左のグラフを描くためのキャッシュ
    int dotperstep=1;		// 左のグラフで, 時間1ステップにずれる空間的幅
    int [] xcache;		// 過去の座標のキャッシュ
    int [] ycache;		// 過去の座標のキャッシュ
    boolean [] cacheused;	// 有効なキャッシュか?
    final int ncache=200;	// キャッシュのサイズ
    int ic=0;			// ncache個のキャッシュのうち現在 ic 番目

    // グラフィックス    
    Graphics offgr;		// double buffering 用

    int unitradius;		// 画面に収まる最大の振幅/2(dot 数)
    int x0,y0;			// 原点の座標
    final int bsize=1;   // bsize+0.5 == 運動するボールの半径(ドット)

    // Panel/Form
    final String formtitle="振幅,角速度,初期位相";

    final String infotitle="リサジュー図形";
    final String infotext
	="http://hig3.net" + "\n" + 
	"(c)2004 hig" + "\n" + 
	"龍谷大学数理情報学科\n"+
	"MathFP by Onno Hommes使用";

    TextBox  [] bi;

    Label  [] bititle;       // bi のタイトル
    // MIDP では TextField の Label を使えばよい.

    // コマンド/ソフトキーのラベル

    final String[] cctitle = {"設定", "終了"}; // Canvasの時
    final String[] cftitle = {"アニメ", "情報"}; // Form/Panelの時

    LissajousCanvas(Lissajous m){
	this.parent=m;

	if( getWidth() > getHeight() ){
	    unitradius=getHeight()/4;
	} else {
	    unitradius=getWidth()/4;
	}
	x0 = unitradius*2;
	y0 = unitradius*2;

	// 過去の波動の y 座標の cache の初期化.
	ycache = new int[ncache];
	xcache = new int[ncache];
	cacheused = new boolean[ncache];

	reset();

	// 振動数の初期化
	v = new int [vname.length];
	h= new Harmonic [2];
	h[0] = new Harmonic(MathFP.toFP(unitradius),unity,zero);
	h[1] = new Harmonic(MathFP.toFP(unitradius),unity,MathFP.toFP("0.5"));

	// Canvas でのソフトキー/コマンドキーの設定

	if( cctitle.length > 0 ){
	    setSoftLabel(Frame.SOFT_KEY_1,cctitle[0]);
	    if( cctitle.length > 1 ){
		setSoftLabel(Frame.SOFT_KEY_2,cctitle[1]);
	    }
	}

	// スレッド起動
	if( rt == null ){
	    rt = new Thread(this);
	    rt.start();
	}

    }

    void reset(){
	t=0;
	ic=0;
	realsec=zero;
	for(int i=0; i< cacheused.length; i++){
	    xcache[i]=zero;
	    ycache[i]=zero;
	    cacheused[i]=false;
	}
	inanim=false;
    }

    public void run(){
	while( rt == Thread.currentThread() ){
	    if( inanim ){
		t++;
		ic=(ic+1)%ncache;
		realsec=MathFP.div(MathFP.toFP(t * dt),mille);
		cacheused[ic]=false;

		repaint();
		try {
		    Thread.sleep(dt);
		} catch ( InterruptedException e){
		    break;
		}
	    }
	}
    }

    public void paint(Graphics g){

	final int fheight=Font.getDefaultFont().getHeight();
	final int fwidth=Font.getDefaultFont().stringWidth("8");

	// ダブルバッファリング前処理

	offgr=g;		// これは MIDP と記述をそろえるためだけ
	offgr.lock();		//画面をいったん固定(ダブルバッファリング)
	offgr.clearRect(0,0,getWidth(),getHeight());// 画面を消す

	// 座標軸
	offgr.setColor(Graphics.getColorOfName(Graphics.BLACK) );
	offgr.drawLine(0,y0,getWidth(),y0);// x axis
	offgr.drawLine(x0,0,x0,getHeight());// y axis

	// ラベル
	offgr.drawString("x",getWidth()-fwidth,y0+fheight +0 );
	offgr.drawString("y",x0-fwidth,fheight+fheight +0 );

	int x=h[0].getCoordinate(realsec);
	int y=h[1].getCoordinate(realsec);

	

	xcache[ic]=x;
	ycache[ic]=y;
	cacheused[ic]=true;

	// 粒子と射影
	offgr.setColor(Graphics.getColorOfName(Graphics.BLUE) );
	offgr.fillRect(x0 + x-bsize, y0-y-bsize,bsize+bsize+1,bsize+bsize+1);
	offgr.fillRect(x0    -bsize, y0-y-bsize,bsize+bsize+1,bsize+bsize+1);
	offgr.fillRect(x0 + x-bsize, y0-0-bsize,bsize+bsize+1,bsize+bsize+1);
	offgr.setColor(Graphics.getColorOfName(Graphics.AQUA) );
	offgr.drawLine(x0+0,y0-0,x0+x,y0-y);
	offgr.setColor(Graphics.getColorOfName(Graphics.YELLOW) );
	offgr.drawLine(x0+0,y0-y,x0+x,y0-y);
	offgr.drawLine(x0+x,y0-0,x0+x,y0-y);

	// キャッシュの情報から軌跡を描く
	{
	    int ii=0;
	    int jj=ii+1;
	    offgr.setColor(Graphics.getColorOfName(Graphics.BLUE) );
	    while(jj < ncache-1 && ii < ncache-2){
		if(cacheused[(ncache-ii+ic+ncache)%ncache]==true){
		    jj=ii+1;
		    while( jj < ncache){
			if( cacheused[(-jj+ic+ncache)%ncache]==true){
			    offgr.drawLine(x0+xcache[(-ii+ic+ncache)%ncache],y0-ycache[(-ii+ic+ncache)%ncache],
					   x0+xcache[(-jj+ic+ncache)%ncache],y0-ycache[(-jj+ic+ncache)%ncache]);

			    ii=jj;
			    break;
			} else {
			    jj++;
			}
		    }
		} else {
		    ii++;
		}
			
	    }
	}

    // 説明
        offgr.setColor(Graphics.getColorOfName(Graphics.BLACK) );
	offgr.drawString("リサジュー図形",0,fheight +0 );
	offgr.drawString("start/pause: 選択キー",0,getHeight() +0 );

	// ダブルバッファリング後処理

	offgr.unlock(true);		

    }

    // フォームを表示する.
    public void showPanel(){

	Panel  p;

	p =  new Panel ();
	p.setTitle(formtitle);

	for(int j=0; j<formula.length ; j++){
	    p. add (new Label ("" +  formula[j]));

	    Label  separator=new Label ("");
 	    separator.setSize(p.getWidth(),1);
	    p. add (separator);

	}

	bi = new TextBox [v.length];

	bititle = new Label [ v.length ];

	    v[0]=h[0].getAmplitude();
	    v[1]=h[0].getAngularVelocity();
	    v[2]=h[0].getInitialPhase();
	    v[3]=h[1].getAmplitude();
	    v[4]=h[1].getAngularVelocity();
	    v[5]=h[1].getInitialPhase();

	for(int j=0; j< v.length; j++){
	    String tmptitle=vname[j]+"=";
	    String tmpvalue=MathFP.toString(v[j]);

	    bititle[j] = new Label (tmptitle);
	    bi[j] = new TextBox (tmpvalue,18,1,TextBox.DISPLAY_ANY);
	    bi[j].setEditable(true);
	    p. add (bititle[j]);
	    p. add (bi[j]);

	    Label  separator=new Label ("");
	    separator.setSize(p.getWidth(),1);
	    p. add (separator);

	}

	    

	if( cftitle.length > 0 ){
	    p.setSoftLabel(Frame.SOFT_KEY_1,cftitle[0]);
	    if( cftitle.length > 1 ){
		p.setSoftLabel(Frame.SOFT_KEY_2,cftitle[1]); 
	    }
	}
	p.setSoftKeyListener(this);
	p.setComponentListener(this);
	Display.setCurrent(p);

    }

    void readBoxes(){
	for(int k=0; k< v.length ; k++){
		v[k]=MathFP.toFP(bi[k]. getText ());
	    }
    }

    void setParams(){
	reset();
	h[0].setAmplitude(v[0]);
	h[0].setAngularVelocity(v[1]);
	h[0].setInitialPhase(v[2]);
	h[1].setAmplitude(v[3]);
	h[1].setAngularVelocity(v[4]);
	h[1].setInitialPhase(v[5]);
    }

    // パネルの時
    public void componentAction(Component source,  int type, int param){
	if ( type==TEXT_CHANGED /* && source==bi[something] */){
	    readBoxes();
	}
    }

    public void softKeyPressed(int key){
	if( key==Frame.SOFT_KEY_1 ){
	    setParams();
	    Display.setCurrent(this);
	} else if ( key==Frame.SOFT_KEY_2){
	    Dialog info= new Dialog(Dialog.DIALOG_INFO,infotitle);
	    info.setText(infotext);
	    info.show();
	}

    }

    public void softKeyReleased(int key){
    }

    // キャンバスの時
    public void processEvent(int type, int param){
	if( type==Display.KEY_PRESSED_EVENT ){
	    switch( param ){
	    case Display.KEY_SELECT:
		inanim=!inanim;
		break;
	    case Display.KEY_SOFT1:
		inanim=false;
		showPanel();
		break;
	    case Display.KEY_SOFT2:
		parent.terminate();
	    default:
	    }
	} 
	    
    }

}

class Harmonic {
    
    private int amplitude;
    private int angularVelocity;
    private int initialPhase;
    
    
    public	Harmonic(int amplitude,int angularVelocity,int initialPhase){
	    this.amplitude=amplitude;
	    this.angularVelocity=angularVelocity;
	    this.initialPhase=initialPhase;
    }
    public void setAmplitude(int a){ this.amplitude=a; }
    public void setAngularVelocity(int v){ this.angularVelocity=v; }
    public void setInitialPhase(int p){ this.initialPhase=p; }
    public int getAmplitude(){ return  amplitude; }
    public int getAngularVelocity(){ return angularVelocity; }
    public int getInitialPhase(){ return initialPhase; }
    
    public int getCoordinate(int t){
	return MathFP.toInt(MathFP.mul(amplitude,MathFP.cos( MathFP.add(MathFP.mul(angularVelocity, t),
									MathFP.mul(initialPhase,MathFP.PI)))));
    }

}

// Local Variables:
// mode: java
// compile-command: "make -k Lissajous.java im/Lissajous.java ez/Lissajous.java vf/Lissajous.java"
// End:

