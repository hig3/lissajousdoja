固定小数点パッケージMathFPを使ったiアプリ/Vアプリ/EZアプリのサンプル

Time-stamp: "2004/08/10 Tue 10:31  hig"
樋口三郎 Saburo Higuchi http://hig3.net

make -f Makefile.simple で, Lissajous.javapp から

./Lissajous.java   
(cpp -P -C -DDOJA あるいは cpp -P -C -DMIDP または DoJa/MIDP Builder のプリプロセッサで処理されるべきファイル)

im/Lissajous.java  (iアプリ用)
ez/Lissajous.java  (EZアプリ用)
vf/Lissajous.java  (Vアプリ用 現在は EZ アプリ用と同じです)
が生成されます.


MathFP は Onno Hommes さんの作成されたクラスライブラリです.
http://home.rochester.rr.com/ohommes/

MathFPを用いたiアプリ/Vアプリ/EZアプリをビルドする方法は
以下でも説明されています.
http://sparrow.math.ryukoku.ac.jp/~hig/info/mathfp/

MathFPを使ったiアプリ/Vアプリ/EZアプリは以下にもあります.
http://sparrow.math.ryukoku.ac.jp/~hig/mobilejava/examples/
